# ------------------------------------------------------------------------------------
# filename: kDiskCleaner.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://klevstul.com)
# started:  14.10.2004
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
# version:  v01_20100410
#          - List module version now also shows module config and not just module names
# version:  v01_20100306
#          - Added "-l" mode to list modules in ini file.
#          - Added check to prevent using the "FORBIDDEN" module (it is a reserved module)
# version:  v01_20041113
#          - Fixed program crash that sometimes occured when filename contained regExp
#            characters.
# version:  v01_20041021
#           - Fixed a bug in wildcard check that caused '.' to be treated as a reg.exp.
#           - Fixed a minor bug in printing of the error message.
# version:  v01_20041016
#           - The first version of this program was released.
# ------------------------------------------------------------------------------------


# -------------------
# use
# -------------------
use strict;


# -------------------
# declarations
# -------------------
my $version 				= "v01_20100410";
my $help 					= "\nkDiskCleaner [parameters] [path]\n--- Parameters: ---\n-m [module name] : specify which module, from the '.ini' file, to use\n-r               : run recursively through all sub directories\n-s               : run in silent mode\n-v               : run in verbose mode\n-l               : list available modules\n-ver             : show version number\n-help            : display this help\n--- - - - - - - ---\nExample:\nkDiskCleaner -m mac -r -v M:\\\n";
my $heading 				= "kDiskCleaner> ";

my $verbose 				= 0;																	# verbose mode, on/off
my $module 					= 0;																	# module to run
my $root 					= 0;																	# the root from where all directories and files will be checked
my $recursively 			= 0;																	# runs recursively through all directories
my $silent 					= 0;																	# runs in silent mode, without asking any questions or outputing any information
my $list_modules			= 0;																	# list available modules in init file

my $files_to_delete;																				# the files to delete will be stored in this variable
my $no_files_to_delete		= 0;																	# number of files to be deleted
my $directories_scanned		= 1;																	# the number of directories that are scanned

my $path 					= &getPath;																# retrieve the path to where this program is running
my $iniFile					= $path . "kDiskCleaner.ini";											# the file containing all configuration values

my $debug 					= 0;																	# debug mode, on/off
my $disable_delete			= 0;																	# turn this value on if you want to disable the delete commands, for debugging use only


# -------------------
# main program
# -------------------
getArguments();																						# get all arguments from command line
loadValues();																							# load values from .ini file
scanDisk($root);																					# scan the disk
if (!$silent){																						# unless we are in silent mode ask the user before we delete the files
	confirmDelete();
} else {
	scanDisk($root,1);																				# the files will be deleted automatically
}





# -------------------
# sub
# -------------------

# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Get arguments from user
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getArguments{
	my $arg;
	my $m_read = 0;																					# if we have read the parameter '-m' this becomes "true", then we know that this paramter's value will come next

	foreach $arg (@ARGV){
		if ($arg =~ m/-help/){																		# print out help and exit program
			kPrint($help);
			exit;
		} elsif ($arg =~ m/-ver/){
			kPrint("$version");
			exit;
		} elsif ($arg =~ m/^-/ || $m_read ) {														# if we've got a parameter, or waiting to read a parameters value
			if ($arg eq "-v"){
				$verbose = 1;
				kDebug("Verbose mode is ON");
			} elsif ($arg eq "-m"){
				$m_read = 1;
			} elsif ($arg eq "-r"){
				$recursively = 1;
				kDebug("Program runs recursively though all directories");
			} elsif ($arg eq "-s"){
				$silent = 1;
				kDebug("Program runs in silent mode");
			} elsif ($m_read && $arg){																# the '-m' parametr was read during previous loop, and here comes the value...
				$module = $arg;
				$m_read = 0;
				if ($module =~ m/^FORBIDDEN$/i){
					kError("The module '$module' is a reserved module", 1);
				}
				kDebug("'$module' module");
			} elsif ($arg eq "-l"){
				$list_modules = 1;
				kDebug("List modules mode is ON");
				getIniValue(0,1);
				exit;
			} else {																				# unrecorgnised argument
				kError("Unrecorgnised argument '$arg' skipped");
			}
		} else {																					# the path to the root should come here
			if (!$root){
				$root = $arg;
				kDebug("Program will run in '$arg'");
			} else {
				kError("The start path has already been given, '$arg' is ignored");
			}
		}
	}

	if (!$root){																					# check if user has specified a root directory where this program will "run" or check files
		kError("Please specify the root where you want this program to run");
		kPrint($help);
		kError("Please specify the root where you want this program to run",1);
	} else {
		$root =~ s/\\$//;																			# in case user typed in '\' or '/' after the directory remove these values
		$root =~ s/\/$//;

		my $forbidden_dirs = getIniValue("FORBIDDEN");												# loads the forbidden directories from the .ini file
		my @forbidden_dirs = split(",", $forbidden_dirs);											# split on ',' and add all in an array
		kDebug($forbidden_dirs);

		foreach $_ (@forbidden_dirs){																# goes through all directories
			$_ =~ s/\"//g;																			# removes '"'
			$_ =~ s/\s//g;																			# removes spaces
			$_ =~ s/\\$//;																			# in case user typed in '\' or '/' after the directory remove these values
			$_ =~ s/\/$//;
			$_ =~ s/\\/\\\\/g;																		# replace '\' with '\\', otherwise there will be reg.exp. conflict

			if ($root =~ m/$_/){																	# if the specified root directory contains a forbidden directory exit program
				kError("Directory chosen, '$root', conflicts with a forbidden dir specified in the .ini file",1);
			}
		}
	}

	if (!-e $root){																					# checks that the directory exists
		kError("Invalid root: $root",1);
	}

	if (!$module){																					# check that user has specified a module
		kError("Please specify the module, specified in the '.ini' file you want to use. List modules using the -l option");
		kPrint($help);
		kError("Please specify the module, specified in the '.ini' file you want to use. List modules using the -l option");
	}
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Load values from .ini file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub loadValues{
	$files_to_delete = getIniValue($module);														# load the values that can be deleted from '.ini' file
	kDebug($files_to_delete);
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Check if the arguments are valid, and load values from .ini file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub scanDisk{
	my $directory 		= $_[0];																	# the directory where the files will be scanned
	my $do_delete 		= $_[1];																	# if true then delete the files, if not ask user first
	my $clean_directory = $_[2];																	# if true we'll delete all the files in the directory

	my @files;
	my $file;
	my $file_regClean;
	my $_files_to_delete;																			# we store the files to delete in a local variable since we have to manipulate this variable when we want to delete everything in a directory
	my @_files_to_delete;																			# store all files to delete in an array as well, this array is to be used when processing '*' (wildcard) in a filename
	my $index;																						# used for checking the position of the wildcard, '*', in a filename

	# ----------------------------------------
	# goes through a directory and "fetches"
	# all files and directories
	# ----------------------------------------
	opendir (D, "$directory");
	@files = grep /\w/, readdir(D);
	closedir(D);

	kDebug("Dir: $directory");																		# print out (debug mode only) the directory we're in

	# ----------------------------------------
	# goes through all the files in the directory
	# ----------------------------------------
	foreach $file (@files){
		$file_regClean	= &commentRegExpChars($file);												# comment out all reg exp characters

		$_files_to_delete = $files_to_delete;														# resets the variable since it gets modified each round/loop

		if ($clean_directory){																		# if we want to clean a directory we want all files to match
			$_files_to_delete = "\"".$file."\"";
		}


		# ----------------------------------------
		# process wildcard '*' in filename to be deleted
		# ----------------------------------------
		@_files_to_delete = split(",", $_files_to_delete);

		foreach (@_files_to_delete){																# go through all files to delete
			if ($_ !~ m/\*/){																		# we're not interested if the filname to delete doesn't contian a '*' (wildcard)
				next;
			}

			$_ =~ s/^\s{1}//g;																		# remove the first space, that occurs due to the split
			$_ =~ s/"//g;																			# remove all '"'
			$_ =~ s/\./\\./g;																		# replacing '.' with '\.'
			$index = index($_,"*");																	# find the position of '*'
			if ($_ eq "*"){																			# we don't want to match all files, too dangerous...
				next;
			}
			$_ =~ s/\*//g;																			# remove '*'

			if ($index == 0){																		# if index == 0 the wildcard '*' was the first character, which means that the filname $file has to end with the same as $_ (so $_$)
				if ($file =~ m/$_$/){
					$_files_to_delete = "\"".$file."\"";											# we "trick" this to be a match
				}
			} else {																				# otherwise the wildcard is assumed to be at the end of the filename (we don't support '*' in the middle of the filename)
				if ($file =~ m/^$_/){
					$_files_to_delete = "\"".$file."\"";
				}
			}
		}


		# ----------------------------------------
		# check if we have a match
		# ----------------------------------------
		my $tmp = &commentRegExpChars($file);
		if ($_files_to_delete =~ m/"$file_regClean"/){												# if the file we are checking now is one of the files we want to delete...
			if (!$do_delete && $verbose){															# if we're just going through the files in verbose mode, print out info
				if (-d "$directory/$file"){
					kPrint("The directory '$directory/$file' with all it's content");
				} else {
					kPrint("The file '$file' from '$directory'");
				}
			}

			if ($do_delete){																		# the user has accepted to delete the files (or we're in silent mode where $do_delete == 1)
				if (-d "$directory/$file"){															# if the file is a directory
					&scanDisk("$directory/$file", $do_delete, 1);									# clean/empty the directory, passing a third argument (1) will take care of that
					if (!$disable_delete){
						rmdir "$directory/$file";													# after the directory is clean we can remove it
					}
					if ($verbose){
						kPrint("Directory '$directory/$file' deleted");
					}
				} else {																			# the file is not a directory, just a plain file
					if (!$disable_delete){
						unlink "$directory/$file";													# remove or unlink this file from the system
					}
					if ($verbose){
						kPrint("File '$file' deleted from '$directory'");
					}
				}
			}
			$no_files_to_delete++;																	# increase the counter that tells us how many files we're going to delete
		}


		# ----------------------------------------
		# recursivity?
		# ----------------------------------------
		if ($recursively && $_files_to_delete !~ m/"$file_regClean"/){								# if recursively and this filename matches any of the files we want to delete
			if (-d "$directory/$file"){																# if the file is a directory
				$directories_scanned++;
				&scanDisk("$directory/$file", $do_delete);											# run recursivly down through all directories
			}
		}
	}
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Ask user before the files will be deleted
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub confirmDelete{
	my $input;

	my $dirtext;
	if ($directories_scanned == 1){
		$dirtext = "directory";
	} else {
		$dirtext = "directories";
	}

	if ($no_files_to_delete > 0){
		kPrint("A total of $no_files_to_delete files and directories were identified in the $directories_scanned $dirtext scanned.");
		kPrint("Do you want to delete all these files? Type 'yes' to confirm");
		print "$heading";
		chop($input=<STDIN>);																			# ask user for input

		if ($input eq "yes"){																			# if user types yes
			&scanDisk($root,1);																			# delete the files
		} else {
			exit;
		}
	} else {
		kPrint("No files were found.");
		exit;
	}
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Prints out message on screen
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kPrint{
	my $text = $_[0];

	$text =~ s/\n/\n$heading/sgi;

	if (!$silent){
		print $heading ."". $text . "\n";
	}
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Prints out debug message
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kDebug{
	my $text = $_[0];
	my $silent_turned_off = 0;

	if ($debug){
		if ($silent){																				# turns off silent mode to print debug information
			$silent = 0;
			$silent_turned_off = 1;
		}
		&kPrint("DEBUG: ".$text);
		$silent = 0;

		if ($silent_turned_off){																	# if we have turned the silent mode off, turn it back on
			$silent = 1;
			$silent_turned_off = 0;
		}
	}
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Prints out error message
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kError{
	my $text = $_[0];
	my $exit = $_[1];

	kPrint("ERROR: ".$text);

	if ($exit){
		exit;
	}
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load initial config values
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getIniValue{
	my $parameter	= $_[0];
	my $list		= $_[1];
	my $line;
	my $value;

	$parameter =~ tr/a-z/A-Z/;																		# convert the parameter to uppercase

    open(FILE, "<$iniFile") || kError("failed to open '$iniFile'");
	WHILE: while ($line = <FILE>){
		if ($line =~ m/^#/){
			next WHILE;
		}

		if ($list){
			if ($line =~ m/^(\w+)(\t)+(.*)$/){
				print $1 . " : " . $3 . "\n";
			}
		} else {
			if ($line =~ m/^$parameter(\t)+(.*)$/){
				$value = $2;
				last WHILE;
			}
		}
	}
	close (FILE);

	if ($value eq "" && !$list){
		kError("Failed to find the values for module '$parameter' in '$iniFile'",1);
	}

	kDebug("'$parameter' values read from .ini file");

	return $value;
}



# - - - - - - - - - - - - - - - - - - - - - - - - - -
# find the path to where the program is running
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getPath{
	$0=~/^(.+[\\\/])[^\\\/]+[\\\/]*$/;
	my $cgidir = $1 || "./";
	return $cgidir;
}



# - - - - - - - - - - - - - - - - - - - - - - - - - -
# comment out all reg exp characters
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub commentRegExpChars{
	my $string = $_[0];

	# ----------------------------------------
	# comment out the following characters:
	# \  ^  .  $  |  (  )  [  ] *  +  ?  {  }  ,
	# ----------------------------------------

	$string =~ s/\\/\\\\/sg;
	$string =~ s/\^/\\\^/sg;
	$string =~ s/\./\\\./sg;
	$string =~ s/\$/\\\$/sg;
	$string =~ s/\|/\\\|/sg;
	$string =~ s/\(/\\\(/sg;
	$string =~ s/\)/\\\)/sg;
	$string =~ s/\[/\\\[/sg;
	$string =~ s/\]/\\\]/sg;
	$string =~ s/\*/\\\*/sg;
	$string =~ s/\+/\\\+/sg;
	$string =~ s/\?/\\\?/sg;
	$string =~ s/\{/\\\{/sg;
	$string =~ s/\}/\\\}/sg;
	$string =~ s/\,/\\\,/sg;

	return $string;
}

